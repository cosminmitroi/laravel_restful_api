<?php
/**
 * Created by CosminM.
 * Date: 15.11.2015
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use \App\Repositories\UserRepository;
use \App\User as Users;

class AuthenticateController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

    /**
     * Return a JWT
     *
     * @return Response
     */
    public function authenticate(Request $request, UserRepository $userRepository)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        // Fetch user entity
        $oUserEntity = $userRepository->getByEmail($credentials['email']);

        // Check if user is active, otherwise abort authentication
        if($oUserEntity->getStatus() == 'inactive') {
            return response()->json(['error' => 'user_inactive'], 401);
        }

        // Add user role name into jwt token
        $customClaims = ['roleName' => implode(',', $oUserEntity->getRoles())];
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials, $customClaims)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], $e->getStatusCode());
        }

        // all good so return the token
        return response()->json(['token' => $token, 'userRoles' => $oUserEntity->getRoles()]);
    }

    /**
     * Return the authenticated user
     *
     * @return Response
     */
    public function getAuthenticatedUser()
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['token_expired'], 401);
        } catch (TokenInvalidException $e) {
            return response()->json(['token_invalid'], 401);
        } catch (JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }
}
