<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Collection;
use \Illuminate\Http\Request;
use \App\Http\Requests;
use \App\Http\Controllers\Controller;
use \App\Factories\ContractorFactory;
use \App\Repositories\ContractorRepository;
use \App\Exceptions\NotFound;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Entities\ContractorEntity[]
     */
    public function index(ContractorRepository $oContractorRepository)
    {
        return $oContractorRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Entities\ContractorEntity
     */
    public function store(Request $request, ContractorRepository $oContractorRepository)
    {
        $oContractorEntity = ContractorFactory::makeFromArray($request->all());
        if ($oContractorEntity->getId() == null) {
            return $oContractorRepository->store($oContractorEntity);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \App\Entities\ContractorEntity|\App\Exceptions\NotFound
     */
    public function show($iContractorEntity, ContractorRepository $oContractorRepository)
    {
        return $oContractorRepository->getById($iContractorEntity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $oRequest, $iContractorEntity, ContractorRepository $oContractorRepository)
    {
        $oContractorEntity = ContractorFactory::makeFromArray($oRequest->all());

        if ($oContractorEntity) {
            $oContractorEntity->setId($iContractorEntity);
            if (intval($iContractorEntity) != 0) {
                return $oContractorRepository->store($oContractorEntity);
            }
        } else {
            throw new NotFound('ContractorEntity not found.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($iContractorEntity, ContractorRepository $oContractorRepository)
    {
        $oContractorEntity = $oContractorRepository->getById($iContractorEntity);
        if ($oContractorEntity) {
            return $oContractorRepository->delete($oContractorEntity);
        }
        throw new NotFound('ContractorEntity not found.');
    }
}
