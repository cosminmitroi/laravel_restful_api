<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Collection;
use \Illuminate\Http\Request;
use \App\Http\Requests;
use \App\Http\Controllers\Controller;
use \App\Factories\UserFactory;
use \App\Repositories\UserRepository;
use \App\Exceptions\NotFound;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Entities\UserEntity[]
     */
    public function index(UserRepository $oUserRepository)
    {
        return $oUserRepository->getAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Entities\UserEntity
     */
    public function store(Request $request, UserRepository $oUserRepository)
    {
        $oUserEntity = UserFactory::makeFromArray($request->all());
        if ($oUserEntity->getId() == null) {
            return $oUserRepository->store($oUserEntity);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \App\Entities\UserEntity|\App\Exceptions\NotFound
     */
    public function show($iUserId, UserRepository $oUserRepository)
    {
        return $oUserRepository->getById($iUserId);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $oRequest, $iUserId, UserRepository $oUserRepository)
    {
        $oUserEntity = UserFactory::makeFromArray($oRequest->all());

        if ($oUserEntity) {
            $oUserEntity->setId($iUserId);
            if (intval($iUserId) != 0) {
                return $oUserRepository->store($oUserEntity);
            }
        } else {
            throw new NotFound('UserEntity not found.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($iUserId, UserRepository $oUserRepository)
    {
        $oUserEntity = $oUserRepository->getById($iUserId);
        if ($oUserEntity) {
            return $oUserRepository->delete($oUserEntity);
        }
        throw new NotFound('UserEntity not found.');
    }
}
