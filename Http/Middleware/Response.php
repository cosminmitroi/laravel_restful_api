<?php

namespace App\Http\Middleware;

use \Illuminate\Http\Response as IlluminateResponse;
use \Illuminate\Support\Collection;
use \App\Entities\UserEntity;
use \App\Entities\RepairLogEntity;
use \App\Entities\FileEntity;
use \App\Entities\ContractorEntity;
use \App\Entities\DocumentEntity;
use \App\Exceptions\NotFound;
use \App\Exceptions\UnprocessableEntity;
use \Closure;

class Response
{
	private static $aValidResponseTypes = [
        Collection::class,
		UserEntity::class, RepairLogEntity::class, FileEntity::class,
        ContractorEntity::class, DocumentEntity::class
	];

    public function handle($oRequest, Closure $oNext)
    {
        $oResponse = $oNext($oRequest);

        // Process the response
        if(is_object($oResponse->original)) {
            $sResponseClass = get_class($oResponse->original);
            if(in_array($sResponseClass, static::$aValidResponseTypes)) {
                $oReflection = new \ReflectionClass($sResponseClass);
                $sMethod = lcfirst($oReflection->getShortName());

                return static::$sMethod($oResponse);
            }
        }
        return $oResponse;
    }

    private function userEntity(IlluminateResponse $oResponse) {
        return $this->entityJsonPrinter($oResponse);
    }

    private function repairLogEntity(IlluminateResponse $oResponse) {
        return $this->entityJsonPrinter($oResponse);
    }

    private function fileEntity(IlluminateResponse $oResponse) {
        return $this->entityJsonPrinter($oResponse);
    }

    private function contractorEntity(IlluminateResponse $oResponse) {
        return $this->entityJsonPrinter($oResponse);
    }

    private function documentEntity(IlluminateResponse $oResponse) {
        return $this->entityJsonPrinter($oResponse);
    }

    private function entityJsonPrinter(IlluminateResponse $oResponse) {
        $sJSON = json_encode(static::convertToArray($oResponse->original));
        $oResponse->setContent($sJSON);

        $oResponse->header('Content-Type', 'application/json');
        return $oResponse;
    }

    private function collection(IlluminateResponse $oResponse) {
        $aResponse = [];
        foreach($oResponse->original as $oEntity) {
            $aResponse[] = static::convertToArray($oEntity);
        }

        $oResponse->setContent(json_encode($aResponse));
        $oResponse->header('Content-Type', 'application/json');
        return $oResponse;
    }

    private static function convertToArray($oEntity) {
        $aData = $oEntity->toArray();

        // Check if this entity has a Dependency Handler
        if(($sHandler = $oEntity->getDependencyHandlerClass()) !== null) {
            $aData = $sHandler::process($aData);
        }

        return $aData;
    }
}