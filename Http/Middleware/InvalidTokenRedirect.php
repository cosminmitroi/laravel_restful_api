<?php

namespace App\Http\Middleware;

use Closure;

class InvalidTokenRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        /**
         * If the request was not made to the API and the user is not autenticated,
         * redirect him to the login page.
         */
        if(!$request->is('api/*')) {
            $aResponse = json_decode($response->content());
            if(isset($aResponse->error)) {
                $response = redirect()->route('login');
            }
        }

        return $response;
    }
}
