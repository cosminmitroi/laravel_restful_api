<?php
/**
 * Created by CosminM.
 */
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use JWTAuth;

class CheckRole
{
    protected $auth;
    /**
     * Creates a new instance of the middleware.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure $next
     * @param  String   $sRequiredRole
     * @return mixed
     * @throws \Tymon\JWTAuth\Exceptions\TokenInvalidException
     */
    public function handle($request, Closure $next, $sRequiredRole = 'superadmin')
    {
        $sRoles = JWTAuth::getPayload()->toArray()['roleName'];
        $aRoles = explode(',', $sRoles);

        if(!in_array($sRequiredRole, $aRoles)) {
            throw new TokenInvalidException();
        }

        return $next($request);
    }
}
