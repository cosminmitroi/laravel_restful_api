<?php
/**
 * Created by CosminM.
 * Date: 09.11.2015
 */

namespace App\Exceptions;

use Exception;

class PermissionDoesNotExist extends Exception
{
    /**
     * Create a new permission denied exception instance.
     *
     * @param string $permission
     */
    public function __construct($permission)
    {
        $this->message = sprintf("You don't have a required ['%s'] permission.", $permission);
    }
}