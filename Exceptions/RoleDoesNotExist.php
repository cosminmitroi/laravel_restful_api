<?php
/**
 * Created by CosminM.
 * Date: 09.11.2015
 */

namespace App\Exceptions;

use Exception;

class RoleDoesNotExist extends Exception
{
    /**
     * Create a new role denied exception instance.
     *
     * @param string $role
     */
    public function __construct($role)
    {
        $this->message = sprintf("You don't have a required ['%s'] role.", $role);
    }
}