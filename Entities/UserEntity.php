<?php
/**
 * Created by CosminM.
 * Date: 12.11.2015
 */
namespace App\Entities;

use App\Entities\Entity;
/**
 * 
 * @package default
 */
class UserEntity extends AbstractEntity implements Entity {
	/**
	 * @var array
	 */
	public static $aRoles = ['superadmin', 'user'];

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $email;

	/**
	 * @var string
	 */
	protected $password;

	/**
	 * @var string
	 */
	protected $status;

	/**
	 * @var array
	 */
	protected $roles = [];

	/**
	 * @param string $sName
	 * @return void
	 */
	public function setName($sName) {
		$this->name = $sName;
	}
	
	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $sEmail
	 * @return void
	 */
	public function setEmail($sEmail) {
		$this->email = $sEmail;
	}
	
	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $sPassword
	 * @return void
	 */
	public function setPassword($sPassword) {
		$this->password = $sPassword;
	}
	
	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $sStatus
	 * @return void
	 */
	public function setStatus($sStatus) {
		$this->status = $sStatus;
	}
	
	/**
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param string $sRole
	 * @return boolean
	 */
	public function pushRole($sRole) {
		if(in_array($sRole, static::$aRoles) && !in_array($sRole, $this->roles)) {
			$this->roles[] = $sRole;
			return true;
		}
		return false;
	}

	/**
	 * @param string $sRole
	 * @return boolean
	 */
	public function removeRole($sRole) {
		if(($iKey = array_search($sRole, $this->roles)) !== false) {
			unset($this->roles[$iKey]);
			return true;
		}
		return false;
	}
	
	/**
	 * @return array
	 */
	public function getRoles() {
		return $this->roles;
	}

	/**
	 * Converts UserEntity to array
	 * 
	 * @return array
	 */
	public function toArray() {
		$aData = [	'id' => $this->id,
					'name' => $this->name,
					'email' => $this->email,
					'status' => $this->status,
					'roles' => $this->roles,
					'created_at' => $this->createdAt ? $this->createdAt->format('Y-m-d H:i:s') : '0000-00-00 00:00:00',
					'updated_at' => $this->updatedAt ? $this->updatedAt->format('Y-m-d H:i:s') : '0000-00-00 00:00:00'];
		if($this->password !== null) {
			$aData['password'] = $this->password;
		}

		return $aData;
	}
}