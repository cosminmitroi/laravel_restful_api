<?php

namespace App\Entities;

interface Entity {
	/**
	 * Sets the id of an entity
	 * 
	 * @param integer $iId 
	 * @return void
	 */
	public function setId($iId);

	/**
	 * Returns the id of an entity
	 * 
	 * @return integer
	 */
	public function getId();

	/**
	 * Converts an Entity to array
	 * 
	 * @return array
	 */
	public function toArray();

	/**
	 * Converts an Entity to string
	 * 
	 * @return string
	 */
	public function __toString();
}